import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 * 
 * @author iam20484311
 * @param
 * @return
 * @exception
 *
 */

public class finestraApp implements ActionListener {

	private JFrame frame;
	private JTextField numA;
	private JTextField numB;
	private JTextField res;
	private JButton suma;
	private JButton resta;
	private JButton mult;
	private JButton div;

	/**
	 * Launches the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					finestraApp window = new finestraApp();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Creates the application.
	 */
	public finestraApp() {
		initialize();
	}

	/**
	 * Initializes the contents of the frame.
	 */
	private void initialize(){
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.LIGHT_GRAY);
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel titol = new JLabel("CALCULADORA");
		titol.setFont(new Font("Dialog", Font.BOLD, 20));
		titol.setBounds(137, 192, 189, 25);
		frame.getContentPane().add(titol);
		
		JLabel primerNumero = new JLabel("Primer Número");
		primerNumero.setBounds(6, 18, 140, 15);
		frame.getContentPane().add(primerNumero);
		
		JLabel segonNumero = new JLabel("Segon Número");
		segonNumero.setBounds(6, 45, 115, 15);
		frame.getContentPane().add(segonNumero);
		
		JLabel lblResultat = new JLabel("Resultat");
		lblResultat.setBounds(20, 91, 115, 19);
		frame.getContentPane().add(lblResultat);
		
		numA = new JTextField();
		numA.setBounds(137, 16, 60, 19);
		frame.getContentPane().add(numA);
		numA.setColumns(10);
		
		numB = new JTextField();
		numB.setBounds(137, 43, 60, 19);
		frame.getContentPane().add(numB);
		numB.setColumns(10);

		res = new JTextField();
		res.setColumns(10);
		res.setBounds(137, 91, 80, 19);
		frame.getContentPane().add(res);
		
		suma = new JButton("+");
		suma.setBounds(240, 13, 80, 25);
		frame.getContentPane().add(suma);
		suma.addActionListener(this);
		
		resta = new JButton("-");
		resta.setBounds(240, 45, 80, 25);
		frame.getContentPane().add(resta);
		resta.addActionListener(this);
		
		mult = new JButton("*");
		mult.setBounds(350, 13, 80, 25);
		frame.getContentPane().add(mult);
		mult.addActionListener(this);
		
		div = new JButton("/");
		div.setBounds(350, 45, 80, 25);
		frame.getContentPane().add(div);
		div.addActionListener(this);
	}
	
	/** 
	 * Calculates and shows the operations
	 */
		public void actionPerformed(ActionEvent e) {
		double num1 = Double.parseDouble(numA.getText());
		double num2 = Double.parseDouble(numB.getText());
		double resultat = 0.0;
		if(e.getSource() == (suma)){
			resultat = num1+num2;
			String resul = String.valueOf(resultat);
			res.setText(resul);
		} else if (e.getSource() == (resta)){
			resultat = num1-num2;
			String resul = String.valueOf(resultat);
			res.setText(resul);
		} else if (e.getSource() == (mult)){
			resultat = num1*num2;
			String resul = String.valueOf(resultat);
			res.setText(resul);
		} else if (e.getSource() == (div)){
			resultat = num1/num2;
			String resul = String.valueOf(resultat);
			res.setText(resul);
		}
		}
}
